//'use strict';

// sticky footer
//-----------------------------------------------------------------------------------
if (ieDetector.ieVersion == 10 || ieDetector.ieVersion == 11) {
  (function () {
    var $pageWrapper = $('#page-wrapper'),
        $pageBody    = $('#page-body'),
        ieFlexboxFix = function () {
          if ($pageBody.addClass('flex-none').height() + $('#header').outerHeight() + $('#footer').outerHeight() < $(window).height()) {
            $pageWrapper.height($(window).height());
            $pageBody.removeClass('flex-none');
          } else {
            $pageWrapper.height('auto');
          }
        };
    ieFlexboxFix();
    $(window).on('load resize', ieFlexboxFix);
  })();
}

// placeholder
//-----------------------------------------------------------------------------------
$(function () {
  $('input[placeholder], textarea[placeholder]').placeholder();
});


// checking if element for page
//-----------------------------------------------------------------------------------
function isOnPage(selector) {
  return ($(selector).length) ? $(selector) : false;
}

function gridActions(h, w){
  this.cursorCoords = [0, 0]; // cursor position holder
  this.turnCounter = 0; // who's turn is it ? -  odd - [X] \ even - [O]
  this.height = h;
  this.width = w;
  this.stateMatrix = [];
  this.lockFlag = 0;

  this.drawGrid = function(gridArray){
    var cursorClass,
      isOccupiedBy,
      isFree;
    $(".js-gamewrap-grid").html("");
    for (var i = 0; i < gridArray.length; i++) {
      for (var j = 0; j < gridArray[i].length; j++) {
        // define styling classes
        cursorClass = (i == 0 && j == 0) ? "mod-cursor" : "";
        gridArray[i][j].occupied ? isFree = "" : isFree = "mod-free";
        if (gridArray[i][j].player) {
          gridArray[i][j].player == 'x' ? isOccupiedBy = "mod-x" : isOccupiedBy = 'mod-o';
        } else {
          isOccupiedBy = "";
        }
        $(".js-gamewrap-grid").append(`<div  data-x='${j}' data-y='${i}' class='${cursorClass} ${isFree} ${isOccupiedBy} cell js-cell'></div>`);
      }
    }
  }

  this.generateMatrix = function(h, w){
      var mat = [], h = this.height, w = this.width;
      for ( var y = 0; y < h; y++ ){
          mat[ y ] = [];
          for ( var x = 0; x < h; x++ ) {
              mat[ y ][ x ] = {"player": false, "occupied": false };
          }
      }
      this.stateMatrix = mat;
      this.drawGrid(this.stateMatrix);
  }

  this.moveCursor = function(evt){
    if (this.lockFlag) return false;
    /*
      arrows keycodes:
      left = 37
      up = 38
      right = 39
      down = 40
      space = 32
    */

    var evt = evt || window.event, pos;

    if (evt.keyCode == '38') {
      if (this.cursorCoords[1]  <= 0) return false;
      this.cursorCoords[1]--;
      changeActiveClass(this.cursorCoords, this.height);
    }

    else if (evt.keyCode == '40') {
      if (this.cursorCoords[1]  >= this.height - 1) return false;
      this.cursorCoords[1]++;
      changeActiveClass(this.cursorCoords, this.height);
    }

    else if (evt.keyCode == '37') {
      if (this.cursorCoords[0]  <= 0) return false;
      this.cursorCoords[0]--;
      changeActiveClass(this.cursorCoords, this.height);
    }

    else if (evt.keyCode == '39') {
      if (this.cursorCoords[0] >= this.width - 1 ) return false;
      this.cursorCoords[0]++;
      changeActiveClass(this.cursorCoords, this.height);
    }

    else if (evt.keyCode == '32') {
      this.putMark(this.cursorCoords, this.height);
    }

    function changeActiveClass(coords, gridHeight){
      // change active class (mod-cursor)
      pos = coords[0] + coords[1] * gridHeight;
      $(".js-cell").removeClass("mod-cursor")
                   .eq(pos)
                   .addClass("mod-cursor");
    }
  }

  this.checkWinner = function(player, state){

    //check horisontal
    function checkHorisontal(player){
      var result = false;
      for (var i = 0; i < state.length; i++) {
        var check = 0;
        for (var j = 0; j < state[i].length; j++) { // comparing with width of grid
          if (state[i][j].player == player) check ++;
        }
        if ( check == state[i].length) {
          result = 1;
        }
      }
      return result;
    }

    //check vertical
    var checkVertical = function(player){
      var result = false;
      for (var i = 0; i < state.length; i++) {
        var check = 0;
        for (var j = 0; j < state[i].length; j++) { // comparing with height of grid
          if (state[j][i].player == player) check ++;
        }
        if ( check == state.length) {
          result = 1;
        }
      }
      return result;
    }

    //check diagonal
    function checkDiag(player){
      var result = false;
      var check = 0;
      for (var i = 0; i < state.length; i++) {
        if (state[i][i].player == player || state[i][state[i].length - i - 1].player == player) check ++;
      }
      if ( check == state.length) {
        result = 1;
      }
      return result;
    }

    // show message in an alert. block cursors.
    if (checkDiag(player) || checkVertical(player) || checkHorisontal(player)) {
      this.lockFlag = 1; // blocks the cursor keys
      $(".mod-cursor").removeClass("mod-cursor");
      alert("The winner is: [" + player + "] !!!");
    }

  }



  this.putMark = function(coords, matrixHeight){
    let player = "x",
        selectedCell = this.stateMatrix[coords[1]][coords[0]];
    if (selectedCell.occupied) return false;
    this.turnCounter ++;
    // check if counter is odd or even
    // if this.turnCounter is odd - it's X turn, else - O turn
    if (this.turnCounter % 2 == 0) player = "o"
    selectedCell.player = player;
    selectedCell.occupied = true;
    this.drawGrid(this.stateMatrix);
    console.log(this.stateMatrix);
    this.checkWinner(player, this.stateMatrix);
  }



}

var game1 = new gridActions(3, 3);
var gridArray = game1.generateMatrix();

document.onkeydown = function(evt){
  game1.moveCursor(evt);
};
